class Defination < ApplicationRecord
  belongs_to :word
  validates :defination_of_words, presence: true
end