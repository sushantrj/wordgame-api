class Word < ApplicationRecord
  has_many :definations
  has_many :synonyms
  has_many :antonyms
  has_many :examples
  validates :name, presence: true

  def examples
    word_examples = super
    arr = []
    word_examples.each do |ex|
      arr.push(ex.word_example)
    end
    return arr
  end

  def definations
    word_definations = super
    arr = []
    word_definations.each do |defin|
      arr.push(defin.defination_of_words)
    end
    return arr
  end

  def synonyms
    word_synonyms = super
    arr = []
    word_synonyms.each do |syn|
      arr.push(syn.word_synonyms)
    end
    return arr
  end

  def antonyms
    word_antonyms = super
    arr = []
    word_antonyms.each do |ant|
      arr.push(ant.word_antonyms)
    end 
    return arr
  end

end

