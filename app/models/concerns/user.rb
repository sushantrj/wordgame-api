class User < ApplicationRecord
  has_many :keylists

  PASSWORD_FORMAT = /\A
  (?=.{8,})          # Must contain 8 or more characters
  (?=.*\d)           # Must contain a digit
  (?=.*[a-z])        # Must contain a lower case character
  (?=.*[A-Z])        # Must contain an upper case character
  (?=.*[[:^alnum:]]) # Must contain a symbol
  /x 
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i 
 
  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :username, presence: true, 
            length: { minimum: 3, maximum: 25 }, 
            uniqueness: { case_sensitive: false }


  validates :email, presence: true,
             uniqueness: { case_sensitive: false } , 
             length: { maximum: 105 }, format: { with: VALID_EMAIL_REGEX } 
            
  before_save { self.email = email.downcase }


  validates :password, 
          presence: true, 
          format: { with: PASSWORD_FORMAT }, 
          confirmation: true

  has_secure_password

end
