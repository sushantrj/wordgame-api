class Example < ApplicationRecord
  belongs_to :word
  validates :word_example, presence: true
  validates :word_id, presence: true
end