class Synonym < ApplicationRecord
  belongs_to :word
  validates :word_synonyms, presence: true
  validates :word_id, presence: true
end