class Category < ApplicationRecord
  validates :name, presence: true, length: { minimum: 7, maximum: 12 }
  validates_uniqueness_of :name
  has_many :api_categories
  has_many :keylist, through: :api_categories
end