class Keylist < ApplicationRecord
  validates :API_KEY, presence: true, uniqueness: true, null: false
  validates :count, presence: true
  belongs_to :user
  has_many :api_categories
  has_many :categories, through: :api_categories
end