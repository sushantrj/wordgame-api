class Antonym < ApplicationRecord
  belongs_to :word
  validates :word_antonyms, presence: true
end