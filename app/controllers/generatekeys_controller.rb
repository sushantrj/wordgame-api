class GeneratekeysController < ApplicationController
  before_action :require_login
    def index
      @val = {}
      @val[:email] = params[:email]
      @val[:username] = params[:username]
      pwd = params[:password]
      @user = User.find_by(username: @val[:username])
      puts @user
      if @user
        if @user.email == @val[:email] && @user.authenticate(pwd)
          @val[:API_KEY] = generate_api_key
          render 'match'
        else
          @word_dict = "User not found or wrong user data"
          render 'return_info/index'
        end
      else
        @word_dict = "User not found or wrong user data"
        render 'return_info/index'
      end
    end

    def create
#      byebug
      keyval = Keylist.new
      keyval.API_KEY = params[:api]
      keyval.count = 0
      keyval.user_id = params[:id]
      if keyval.save
        flash[:notice] = "saved"
        if session[:user_id] != nil
          redirect_to user_keylists_path(session[:user_id])
        else 
          @word_dict = "Key saved!!! login to continue"
          render 'return_info/index'
        end
      else
        flash.now[:alert] = "Something went wrong"
      end
    end

    def discard
      if session[:user_id] == nil
        @word_dict = "Key discarded. Please login to view your account"
        render 'return_info/index'
      else
        redirect_to user_keylists_path(session[:user_id])
      end
    end

    private   #generate api
    def generate_api_key
      SecureRandom.base58(200)
    end

end