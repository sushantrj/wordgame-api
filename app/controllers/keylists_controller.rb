class KeylistsController < ApplicationController
  before_action :require_login
  def index
    @keylist=Keylist.where(:user_id => session[:user_id])
  end

  def destroy #delete a key
    #byebug
    keylist = Keylist.find(params[:id])
    keylist.destroy
    flash[:notice] = "successfully deleted"
    redirect_back(fallback_location: user_keylists_path(session[:user_id]))
  end

  def show #display a key
    @keylist = Keylist.find(params[:id])
  end

end