class ApplicationController < ActionController::Base

  def routing_error(error = 'Routing error', status = :not_found, exception=nil)
    @word_dict="link not valid"+exception.to_s
    render 'return_info/index'
  end

  def action_missing(m, *args, &block)
    @word_dict="link not valid"+exception.to_s
    render 'return_info/index'
  end

  def logged_in?
    if session[:user_id]
      return true
    end
    return false
  end

  def validate_key
    api_key = params[:api_key]
    @keylist_value=Keylist.find_by(API_KEY: api_key)
    @key_exists= !(@keylist_value == nil) 
  end
  
  def validate_word
    @input_word = params[:word]
    @input_word=Word.find_by(:name => @input_word)
    @word_exists= !(@input_word==nil)
  end

  def require_login
    if params[:email] || params[:password] || params[:username]
      return
    end
    unless logged_in?
      flash[:error] = "You must be logged in to access this section"
      session[:return_to_url] = request.url
      redirect_to login_path # halts request cycle
    end
  end

  def increment_count
    @keylist_value.count += 1
    @keylist_value.save
  end

  def authenticate_key
    if !(@key_exists)
      render json: "Invalid Key or Key Expired"
    end
  end

  def authenticate_word
    if !(@word_exists)
      render json: "Word not found"
    end
  end

end