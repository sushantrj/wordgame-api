class SessionsController < ApplicationController
  before_action :require_login, except: [:new, :create]

  def new
  end

  def create  #create login session
    user=User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      session[:email] = user.email
      session[:password] = params[:session][:password]
      session[:username] = user.username
      flash[:notice] = 'Logged in successfully'
      if session[:return_to_url] == 'http://127.0.0.1:3000/admin/generateKey'
        session[:return_to_url] = "http://127.0.0.1:3000/admin/generateKey?email=#{session[:email]}&username=#{session[:username]}&password=#{session[:password]}"
      end
      redirect_to session[:return_to_url] || root_path
    else
      flash.now[:alert] = "There was something wrong with your login details"
      render 'new'
    end
  end

  def destroy #logout and end session
    reset_session
    flash[:notice] = "Logged Out"
    redirect_to root_path
  end

end