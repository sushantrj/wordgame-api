module Api
  module V1
    class OperationsController < ApplicationController

      before_action :validate_key, :validate_word, :authenticate_key, :authenticate_word
      after_action :increment_count, if: -> {@key_exists && @word_exists}

      def definations #fetch definations of word given by user
        render json: Words::DefinationSerializer.new(@input_word).serialized_json
      end

      def examples  #fetch examples of word given by user
        render json: Words::ExampleSerializer.new(@input_word).serialized_json
      end

      def related_words #fetch synonyms and antonyms of word given by user
        render json: Words::RelatedwordSerializer.new(@input_word).serialized_json
      end

    end
  end
end