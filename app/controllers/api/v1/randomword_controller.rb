module Api
  module V1
    class RandomwordController < ApplicationController
      
      before_action :validate_key, :authenticate_key
      after_action :increment_count, if: -> {@key_exists}

      def index   
        offset = rand(Word.count) #generate a random word from db
        random_word = Word.order(Arel.sql('RANDOM()')).first 
        render json: Words::RandomwordSerializer.new(random_word).serialized_json
      end
    
    end
  end
end