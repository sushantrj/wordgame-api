class UsersController < ApplicationController
  before_action :require_login, except: [:new, :create]
  def index
  end

  def new 
    @user = User.new
  end

  def create  #create a new user ie signup
    @user = User.new(params.require(:user).permit(:firstname, :lastname, :username, :email, :password))
    if(@user.save)
      session[:user_id] = @user.id
      session[:username] = @user.username
      session[:email] = @user.email
      session[:password] = params[:user][:password]

      redirect_to user_keylists_path(session[:user_id])
    else
      render 'new'
    end
  end

   def edit   #edit user details
    @user = User.find(params[:id])
   end

  def update
    @user = User.find(params[:id])
    if @user.update(params.require(:user).permit(:firstname, :lastname, :username, :email, :password))
      flash[:notice] = "User Data Updated"
      redirect_to root_path
    else
      render 'edit'
    end
  end

end