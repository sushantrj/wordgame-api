class WordSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name
end
