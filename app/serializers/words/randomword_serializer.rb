class Words::RandomwordSerializer < WordSerializer
  attributes :definations, :examples, :synonyms, :antonyms
end