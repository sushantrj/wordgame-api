class Words::RelatedwordSerializer < WordSerializer
  attributes :synonyms, :antonyms
end