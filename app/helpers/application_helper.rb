module ApplicationHelper

  def current_user
    @current_user ||=User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    if session[:user_id]==nil
      return false
    else
      true
    end
  end

end
