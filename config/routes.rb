Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'welcome#index'
  get 'signup', to: 'users#new'
  
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'  
  resources :users, except: [:new, :show] do
    resources :keylists, only: [:show, :index, :destroy]
  end
  scope '/admin' do
    get 'generateKey', to: 'generatekeys#index'
    post 'generateKey', to: 'generatekeys#create'
    post 'dontgenerate', to: 'generatekeys#discard'
  end

  namespace :api, path: '/' do
    namespace :v1, path: '/' do
      scope '/word' do
        get ':word/definations', to: 'operations#definations'
        get ':word/examples', to: 'operations#examples'
        get ':word/relatedWords', to: 'operations#related_words'
      end
      scope '/words' do
        get 'randomWord', to: 'randomword#index'
      end
    end
  end

  match '*path', :to => 'application#routing_error', via: [:get, :post]

end
