require 'test_helper'

class CategoryTest < ActiveSupport::TestCase

  def setup
    @category=Category.new(name: "Personal")
  end

  test "category should be valid" do    #test a category object is valid or not
    #@category=Category.new(name: "Personal")  #personal use, student/clg level use or professional use
    assert @category.valid?
  end

  test "name should be present" do
    @category.name = " "
    assert_not @category.valid?
  end

  test "name should be unique" do 
    @category.save
    @category2 = Category.new(name: @category.name)
    assert_not @category2.valid?
  end

  test "name should not be too long" do
    @category.name = "a" *100
    assert_not @category.valid?
  end

  test "name should not be too short" do
    @category.name = "a" 
    assert_not @category.valid?
  end

end