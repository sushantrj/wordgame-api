# README

## WordGame API can be used to fetch words and details about the words. Along with the API 
## We can generate various apis for a single user. 

We can Login and Signup to make an account

Things you may want to cover:

* Ruby version 2.6.6

* Run bundle install before running the Web App

* Run 127.0.0.1:3000 to get the website

* Check the routes file to find various paths

* Database used - Sqlite used for development basis

## Basic Routes

* 127.0.0.1:3000 for home page

* 127.0.0.1:3000/signup for signup page

* 127.0.0.1:3000/login for login page

* 127.0.0.1:3000/admin/generateKey?email=email@example.com&username=userexample password=passwordExample to generate api key

* 127.0.0.1:3000/words/randomWord?api_key= to get a random word

* 127.0.0.1:3000/word/:word/definations?api_key= to get defination of a word
