class RemoveWordIdFromWords < ActiveRecord::Migration[6.0]
  def change
    remove_column :words, :word_id, :int
  end
end
