class AddWordIdToSynonyms < ActiveRecord::Migration[6.0]
  def change
    add_column :synonyms, :word_id, :int
  end
end
