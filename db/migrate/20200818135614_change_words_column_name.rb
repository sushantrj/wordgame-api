class ChangeWordsColumnName < ActiveRecord::Migration[6.0]
  def change
    rename_column :words, :words, :name
  end
end
