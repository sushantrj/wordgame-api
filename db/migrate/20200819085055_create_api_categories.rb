class CreateApiCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :api_categories do |t|
      t.integer :api_id
      t.integer :category_id
    end
  end
end
