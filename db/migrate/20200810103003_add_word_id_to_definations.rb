class AddWordIdToDefinations < ActiveRecord::Migration[6.0]
  def change
    add_column :definations, :word_id, :int
  end
end
