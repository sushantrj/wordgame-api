class CreateKeylists < ActiveRecord::Migration[6.0]
  def change
    create_table :keylists do |t|
      t.string :API_KEY
      t.integer :count

      t.timestamps
    end
  end
end
