class AddWordIdToExamples < ActiveRecord::Migration[6.0]
  def change
    add_column :examples, :word_id, :int
  end
end