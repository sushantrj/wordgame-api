class AddUserIdToKeylists < ActiveRecord::Migration[6.0]
  def change
    add_column :keylists, :user_id, :int
  end
end
