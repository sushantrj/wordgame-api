class RenameColumnForApi < ActiveRecord::Migration[6.0]
  def change
    rename_column :api_categories, :api_id, :keylist_id
  end
end
