class AddWordIdToAntonyms < ActiveRecord::Migration[6.0]
  
  def change
    add_column :antonyms, :word_id, :int
  end
end
