# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_19_090256) do

  create_table "antonyms", force: :cascade do |t|
    t.string "word_antonyms"
    t.integer "word_id"
  end

  create_table "api_categories", force: :cascade do |t|
    t.integer "keylist_id"
    t.integer "category_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "definations", force: :cascade do |t|
    t.text "defination_of_words"
    t.integer "word_id"
  end

  create_table "examples", force: :cascade do |t|
    t.text "word_example"
    t.integer "word_id"
  end

  create_table "keylists", force: :cascade do |t|
    t.string "API_KEY"
    t.integer "count"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id"
  end

  create_table "synonyms", force: :cascade do |t|
    t.string "word_synonyms"
    t.integer "word_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "username"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "words", force: :cascade do |t|
    t.string "name"
  end

end
